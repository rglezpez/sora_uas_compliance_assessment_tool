<?php

require_once("databaseHandler.php");
require_once("responseLogic.php");
require_once("crypt.php");
require_once("dataHandler.php");
session_start();

//echo "Does keys exists?\n";
//if(file_exists("private/keys/public.key")) {
//echo "file exists\n";
//} else {
//echo "file does not exist\n";}

//TODO decrypt personal information cookies

$info = json_decode($_SESSION['db_information']);
$answers = json_decode($_COOKIE['db_answers']);

//TODO debug
//  echo "Submitted results:"."<br/>";
//	echo $info->name ."<br/>";
//	echo $info->email."<br/>";
//	echo $info->uav."<br/>";
//	echo $info->sail."<br/>";
//	echo $answers."<br/>";
//	echo "<br/>";

//encrypt name and email for GDPR
$name_encrypted = encrypt($info->name);
$email_encrypted = encrypt($info->email);

//	echo "Encrypted Data <br/>";
//	echo $name_encrypted . "<br/>";
//	echo $email_encrypted . "<br/>";
//
//	echo "<br/> Decrypted Data <br/>";
//	echo decrypt($name_encrypted) . "<br/>";
//	echo decrypt($email_encrypted) . "<br/>";

//Make new database handler
$database = new DatabaseHandler();

// add answers
$database->addAnswerData($name_encrypted, $email_encrypted, $info->uav, $info->sail, $answers);
//$database->createtable();

//close database
$database->closeDbConnection();

?>

<!-- Show results in table -->
<!DOCTYPE html>
<html>
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- Bootstrap stylesheet -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <!-- Custom Stylesheet -->
    <link rel="stylesheet" href="styles.css?v=1">
    <title>SORA V2.0 UAS Assessment Tool: Questionnaire</title>
</head>
<body>
<div class="container">
    <h1>SORA V2.0 UAS Assessment Tool</h1>
    <br/>
</div>

<div class="container col-lg-10">
    <div id="print-result" class="card mb-4">
        <div class="card-header alert alert-info">
            <?php echo "<h2 class='text-center'>Results</h2>"; ?>

        </div>

        <div class="card-body justify-content-center">
            <div class="col-12 d-flex flex-row">

                <!-- Column 1 - Labels -->
                <div class="flex-column">
                    <div class="p-1 font-weight-bold">Name</div>
                    <div class="p-1 font-weight-bold">Email</div>
                    <div class="p-1 font-weight-bold">SAIL</div>
                    <div class="p-1 font-weight-bold">UAV</div>
                </div>

                <!-- Column 2 - data -->
                <div class="flex-column">
                    <div class="p-1"><?php echo $info->name;?></div>
                    <div class="p-1"><?php echo $info->email;?></div>
                    <div class="p-1"><?php echo $info->sail;?></div>
                    <div class="p-1"><?php echo $info->uav;?></div>
                </div>

                <!-- Column for button -->
                <div id="print-button" class="ml-auto flex-column align-self-center">
                    <button id="eval-button" onclick="window.location.href = 'https://docs.google.com/forms/d/e/1FAIpQLSf7QhpNrHl2LOkoyuOWOzOHRhv3cZ_IenAet9_JBF1cpni6yg/viewform?usp=sf_link';" role="button" class="btn btn-success">Click here to evaluate</button>
                    <button onclick="printPDF()" role="button" class="btn btn-primary">Click here to get PDF of results</button>
                </div>
            </div>

            <div class="">
                <table class="table table-striped">
                    <thead>
                    <tr class="d-flex">
                        <th scope="col" class="col-md-1">OSO</th>
                        <th scope="col" class="col-md-5">Question</th>
                        <th scope="col" class="col-md-2">Answer</th>
                        <th scope="col" class="col-md-2">Answer Source</th>
                        <th scope="col" class="col-md-2">Response</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    //Loop though answers and echo them out in table
                    foreach($answers as $ans) {
                        echo "<tr class='d-flex'>";
                        echo '<th class="col-md-1" scope="row">'.$ans->oso.'</th>';
                        echo '<td class="col-md-5">'.$test_data[$ans->oso][decodeQuestionLevel($ans->question_level)][$ans->question][0].'</td>';
                        echo '<td class="col-md-2">'.decodeAnswer($ans->answer).'</td>';
                        echo '<td class="col-md-2">'.decodeSpecificAnswer($ans->answer_specific).'</td>';
                        if($ans->answer == "N"){
                            echo '<td class="col-md-2 text-danger">'.getResponse($ans).'</td>';
                        }
                        else{
                            echo '<td class="col-md-2 text-success">'.getResponse($ans).'</td>';
                        }
                        echo '</tr>';
                    }
                    ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

<!--Lib for making html to PDF pages. Is under MIT license: https://github.com/eKoopmans/html2pdf.js -->
<script src="libs/html2pdf.bundle.min.js"></script>
<script>
    function printPDF() {
        //hide button for snapshot, then show it again
        var button = document.getElementById('print-button');
        button.style.display = 'none';

        var element = document.getElementById('print-result');
        var opt = {
            margin:       1,
            filename:     'sora_results.pdf', //todo Could make this more intelligent to include name, drone name and sail?
            image:        { type: 'jpeg', quality: 0.98 },
            html2canvas:  { scale: 2 },
            pagebreak: { mode: 'avoid-all'},
            jsPDF:        { unit: 'mm', format: 'a4', orientation: 'portrait' }
        };

        // New Promise-based usage:
        html2pdf().set(opt).from(element).save().then(function () {
            //when done show button again
            button.style.display = 'block';
        });

    }
</script>
